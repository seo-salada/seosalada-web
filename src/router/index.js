import Vue from 'vue';

import Router from 'vue-router';
import Home from '@/components/Pages/Home';
import Produto from '@/components/Pages/Produto';
import Busca from '@/components/Pages/Busca';
import Finalizacao from '@/components/Pages/Finalizacao';
import Confirmacao from '@/components/Pages/Confirmacao';
import Login from '@/components/Pages/Login';
import Signup from '@/components/Pages/Signup';
import AboutUs from '@/components/Pages/AboutUs';
import Pedidos from '@/components/Pages/Pedidos';
import DetalhePedido from '@/components/Pages/DetalhePedido';
import MinhaConta from '@/components/Pages/MinhaConta';
import FaleConosco from '@/components/Pages/FaleConosco';
import PageNotFound from '@/components/Pages/PageNotFound';
import EsqueciMinhaSenha from '@/components/Pages/EsqueciMinhaSenha';
import SeoDesconto from '@/components/Pages/SeoDesconto';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/Cadastro/:promoprofile?',
      name: 'Cadastro',
      component: Signup,
    },
    {
      path: '/Pedidos',
      name: 'Pedidos',
      component: Pedidos,
    },
    {
      path: '/DetalhePedido/:pedido',
      name: 'DetalhePedido',
      component: DetalhePedido,
    },
    {
      path: '/Produto/:id',
      name: 'Produto',
      component: Produto,
    },
    {
      path: '/Busca/:palavrachave?/:tipo?/:order?/:labels?',
      name: 'Busca',
      component: Busca,
    },
    {
      path: '/Finalizacao',
      name: 'Finalizacao',
      component: Finalizacao,
    },
    {
      path: '/Confirmacao',
      name: 'Confirmacao',
      component: Confirmacao,
    },
    {
      path: '/SobreNos',
      name: 'AboutUs',
      component: AboutUs,
    },
    {
      path: '/MinhaConta',
      name: 'MinhaConta',
      component: MinhaConta,
    },
    {
      path: '/FaleConosco',
      name: 'FaleConosco',
      component: FaleConosco,
    },
    {
      path: '/EsqueciMinhaSenha',
      name: 'EsqueciMinhaSenha',
      component: EsqueciMinhaSenha,
    },
    {
      path: '/SeoDesconto',
      name: 'SeoDesconto',
      component: SeoDesconto,
    },
    {
      path: '*',
      component: PageNotFound,
    },
  ],
});
