/* global FB*/

// eslint-disable-next-line
window.fbAsyncInit = function () {
  FB.init({
    appId: '1809373992706131',
    cookie: true,
    xfbml: true,
    version: 'v2.8',
  });
  FB.AppEvents.logPageView();
};

const d = document;
const s = 'script';
const id = 'facebook-jssdk';
let js = d.getElementsByTagName(s)[0];
const fjs = d.getElementsByTagName(s)[0];
if (!d.getElementById(id)) {
  js = d.createElement(s); js.id = id;
  js.src = '//connect.facebook.net/en_US/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
}
