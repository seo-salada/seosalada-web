import firebase from 'firebase';

/* eslint-disable no-unused-vars */
const configDev = {
  apiKey: 'AIzaSyDHIrmsZTBEWaRrV9cxYBvCZWVYqznQ6l0',
  authDomain: 'seosalada-staging.firebaseapp.com',
  databaseURL: 'https://seosalada-staging.firebaseio.com',
  projectId: 'seosalada-staging',
  storageBucket: 'seosalada-staging.appspot.com',
  messagingSenderId: '308537957747',
};

const firebaseApp = firebase.initializeApp(configDev);

export default firebaseApp;
