// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueMaterial from 'vue-material';
import VueCarousel from 'vue-carousel';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import FBSignInButton from 'vue-facebook-signin-button';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueFire from 'vuefire';
import VueSession from 'vue-session';
import VueAnalytics from 'vue-analytics';
import VueTippy from 'vue-tippy';
import Datepicker from 'simple-vue-datepicker';
import vueSmoothScroll from 'vue-smooth-scroll';
import VueClipboard from 'vue-clipboard2';

import 'vue-material/dist/vue-material.css';
import 'material-design-iconic-font/dist/css/material-design-iconic-font.css';
import 'font-awesome/scss/font-awesome.scss';
import store from './store/store';

import firebase from './helpers/firebaseConfig';
import App from './App';
import router from './router';
import './assets/css/core.scss';
import Header from './components/Shared/Header';
import Footer from './components/Shared/Footer';
import SectionTitle from './components/Shared/SectionTitle';
import CardSelector from './components/Shared/CardSelector';
import Rating from './components/Shared/Rating';
import Loading from './components/Shared/Loading';

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.use(VueFire);
Vue.use(VueMaterial);
Vue.use(VueCarousel);
Vue.use(FBSignInButton);
Vue.use(VueSession);
Vue.use(vueSmoothScroll);
Vue.use(VueClipboard);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCsBb3ko6H9V395oaQxd-Dx1_A7FK4xOPE',
    libraries: 'places',
  },
});

Vue.use(VueAnalytics, {
  id: 'UA-110884327-1',
  router,
  checkDuplicatedScript: true,
  ecommerce: {
    enabled: true,
    enhanced: true,
  },
});

Vue.component('icon', Icon);
Vue.component('header-bar', Header);
Vue.component('footer-bar', Footer);
Vue.component('section-title', SectionTitle);
Vue.component('card-selector', CardSelector);
Vue.component('loading', Loading);
Vue.component('rating', Rating);
Vue.component('datepicker', Datepicker);
Vue.use(VueFire);
Vue.use(VueTippy);

const db = firebase.database();

Vue.material.registerTheme({
  default: {
    primary: 'green',
    accent: 'light-green',
    warn: {
      color: 'orange',
      hue: '800',
    },
  },
  blue: {
    primary: 'blue',
    accent: 'pink',
  },
  indigo: {
    primary: 'indigo',
    accent: 'pink',
  },
  brown: {
    primary: 'brown',
    accent: 'green',
  },
  purple: {
    primary: 'purple',
    accent: 'blue',
  },
  orange: {
    primary: 'orange',
    accent: 'purple',
  },
  green: {
    primary: 'green',
    accent: 'pink',
  },
  'light-green': {
    primary: 'light-green',
    accent: 'brown',
  },
  'light-blue': {
    primary: 'light-blue',
    accent: 'yellow',
  },
  lime: {
    primary: 'lime',
    accent: 'orange',
  },
  'blue-grey': {
    primary: 'blue-grey',
    accent: 'blue',
  },
  cyan: {
    primary: 'cyan',
    accent: 'pink',
  },
  red: {
    primary: 'red',
    accent: 'pink',
  },
  teal: {
    primary: 'teal',
    accent: 'blue-grey',
  },
  white: {
    primary: 'white',
    accent: 'blue',
  },
  grey: {
    primary: {
      color: 'grey',
      hue: 300,
    },
    accent: 'indigo',
  },
  secondary: {
    primary: 'light-blue',
    accent: 'cyan',
    warn: 'deep-orange',
    background: 'white',
  },
  dashboard: {
    primary: 'red',
    accent: 'indigo',
    warn: 'green',
    background: 'amber',
  },
  graph: {
    primary: 'white',
    accent: 'indigo',
    warn: 'amber',
    background: 'amber',
  },
});

/* eslint-disable no-new */
new Vue({
  router,
  store,
  db,
  created() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('login', user);
      } else if (!this.$session.get('cidade')) {
        // setTimeout(() => {
        //   this.$children[0].$children[0].$refs.locationCheck.open();
        // }, 0);
      }
    });
  },
  el: '#app',
  render: h => h(App),
});
