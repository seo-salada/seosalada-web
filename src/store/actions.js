import firebase from 'firebase';
import moment from 'moment';
import lo from 'lodash';

export default {
  insertToCart({ commit, state }, produto) {
    commit('ADD_TO_CART', produto);
  },
  removeFromCart({ commit, state }, product) {
    commit('REMOVE_FROM_CART', product);
  },
  updateQtdCart({ commit, state }, produto) {
    commit('UPDATE_QTD', produto);
  },
  getProdutos({ commit }) {
    firebase.database().ref('produtos').on('value', (snapshot) => {
      const produtos = lo.map(snapshot.val());
      commit('PRODUTOS', produtos);
    });
  },
  login({ commit, state }, user) {
    firebase.database().ref('clientes')
    .orderByChild('idAuth')
    .startAt(user.uid)
    .endAt(user.uid)
    .limitToFirst(1)
    .once('value', snap => snap.forEach((childSnapshot) => {
      const childData = childSnapshot.val();
      commit('LOGIN', childData);
    }));
  },
  registerWithFacebook({ commit, state }, payload) {
    const cliente = payload;
    cliente.role = 'client';
    firebase.database().ref('clientes').push().set(cliente)
      .then((response) => {
        // eslint-disable-next-line
        console.log(response);
        commit('LOGIN', cliente);
      });
  },
  saveAddress({ commit, state }, payload) {
    const db = firebase.database();
    const clientsRef = db.ref('clientes').child(state.user.id);
    const update = {
      address: payload,
    };
    clientsRef.update(update)
    .then((res) => {
      // eslint-disable-next-line
      console.log(res);
      /* eslint-disable no-param-reassign */
      state.user.address = payload;
      state.newAddress = false;
      /* eslint-disable no-param-reassign */
      state.loading = false;
    });
  },
  submitRating({ commit, state }, rating) {
    const db = firebase.database();
    const rat = rating;
    if (rating.produto && rating.cliente.id) {
      const ref = db.ref(`produtos/${rat.produto}/ratings/${rating.cliente.id}`);
      delete rat.produto;
      ref.set(rat);
    }
  },
  updateRegister({ commit, state }, payload) {
    const db = firebase.database();
    const clientsRef = db.ref('clientes').child(payload.id);

    clientsRef.update(payload)
    .then((res) => {
      // eslint-disable-next-line
      console.log(res);
      /* eslint-disable no-param-reassign */
      state.loading = false;
    });
  },
  register({ commit, state }, payload) {
    const auth = firebase.auth();
    auth.createUserWithEmailAndPassword(payload[0].email, payload[1])
    .then(
      (res) => {
        const db = firebase.database().ref('clientes').push();
        /* eslint-disable no-param-reassign */
        const cliente = payload[0];
        cliente.idAuth = res.uid;
        cliente.role = 'client';
        cliente.id = db.key;
        db.set(payload[0])
        .then((response) => {
          // eslint-disable-next-line
          console.log(response);
          if (payload[2]) {
            const pontos = payload[2];
            const indicador = pontos.usuario;
            firebase.database().ref(`clientes/${cliente.id}/pontos`).push(pontos);
            pontos.usuario = cliente.id;
            pontos.tipo = 'Indicou um amigo';
            firebase.database().ref(`clientes/${indicador}/pontos`).push(pontos);
          }
          state.loading = false;
        });
      },
    ).catch((error) => {
      state.loading = false;
      // eslint-disable-next-line
      console.log(error);
    });
  },
  logout({ commit }) {
    firebase.auth().signOut().then(() => commit('LOGOUT'));
  },
  nextShip({ commit }) {
    moment.locale('pt-br');
    const diaEntrega = 2;
    let dia;
    if (moment().isoWeekday() === 0) {
      dia = moment().isoWeekday(diaEntrega).format('YYYY MM DD');
    } else {
      dia = moment().add(1, 'weeks').isoWeekday(diaEntrega).format('YYYY MM DD');
    }
    commit('NEXT_SHIP', dia);
  },
  newAddress({ commit }, payload) {
    commit('SET_NEW_ADDRESS', payload);
  },
  enviarFaleConosco({ state }, faleConosco) {
    firebase.database().ref('faleconosco').push().set(faleConosco);
  },
  finalizarCompra({ state }, objInfo) {
    const data = moment(new Date()).format('YYYY MM DD');
    const db = firebase.database();
    const somaTotal = state.total;
    const frete = state.frete;
    let promoInfo = {};
    if (state.promoInfo.id) {
      promoInfo = {
        id: state.promoInfo.id,
        nome: state.promoInfo.nome,
        valorMinimo: state.promoInfo.valorMinimo,
        valor: state.promoInfo.valor,
        unidade: state.promoInfo.unidade,
        codigo: state.promoInfo.codigo,
      };
    }
    const pedido = {
      itens: state.cart,
      usuarioID: state.user.id,
      usuarioName: state.user.name,
      usuarioPhone: state.user.phone_number,
      usuarioEmail: state.user.email,
      data,
      frete,
      observacao: objInfo.observacao,
      total: somaTotal,
      subtotal: state.subTotal,
      desconto: state.valorDesconto,
      promocao: promoInfo,
      data_entrega: state.shipmentDate,
      periodo: state.order.period,
      pagamento: state.order.payment,
      troco: objInfo.troco,
      endereco: state.address,
      status: 'Aberto',
      sustentavel: state.sustentavel,
      seoDesconto: state.seoDesconto,
    };
    return new Promise((resolve, reject) => {
      db.ref('ultimoPedido').once('value')
        .then((snap) => {
          const pedidoNumero = Number(snap.val()) + 1;
          pedido.numero = pedidoNumero;
          db.ref('pedidos').push().set(pedido)
          .then(() => {
            db.ref('ultimoPedido').set(pedidoNumero);
            if (state.seoDesconto) {
              state.seoDesconto.forEach((el) => {
                const idponto = el.id;
                db.ref(`clientes/${state.user.id}/pontos/${idponto}/used`).set(true);
              });
            }
            resolve('Sucesso');
          }, (error) => {
            reject(error);
          });
        });
    });
  },
};
