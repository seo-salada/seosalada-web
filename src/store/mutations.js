/* eslint-disable */
import lodash from 'lodash';

export default {
  ADD_TO_CART(state, payload) {
    if (!lodash.find(state.cart, produto => produto.key == payload.key)){      
      state.cart.push(payload);
    }
  },
  REMOVE_FROM_CART(state, cart) {
    state.cart = cart;
  },
  UPDATE_QTD(state, payload){
    const index = lodash.findIndex(state.cart, produto => produto.key == payload.key);
    state.cart[index].qtd = payload.qtd;
    state.cart[index].total = +(state.cart[index].valor * payload.qtd).toFixed(2);
  },
  LOGIN(state, payload) {
    state.user = payload;
    state.logged = true;
  },
  UPDATE_ADDRESS(state, payload) {
    state.address = payload;
  },
  SELECT_PERIOD(state, payload){
    const selectedCommit = payload.commit;
    if(payload.commit == 'period'){
      state.order.period = payload.data;
    }else if(payload.commit == 'payment'){
      state.order.payment = payload.data;
    }
   
  },
  LOGOUT(state){
    state.logged = false;
    state.user = {};
  },
  SET_ADDRESS(state, payload){
    state.address = payload;
  },
  SET_NEW_ADDRESS(state, payload){
    state.newAddress = payload;
  },  
  NEXT_SHIP(state, payload){
    state.nextShip = payload;
  },
  PRODUTOS(state, payload){
    state.produtos = payload;
  },
};
