/* eslint-disable no-param-reassign */
export default {
  SAVE_PRODUTOR(state, payload) {
    state.produtor = payload;
  },
  SAVE_CATEGORIA(state, payload) {
    state.produtor = payload;
  },
  SAVE_IMAGEM(state, payload) {
    state.produtoUrl = payload;
  },
  GET_CLIENTES(state, payload) {
    state.clientes = Object.keys(payload);
  },
  GET_PRODUTOS(state, payload) {
    state.produtos = payload;
  },
  GET_PEDIDO(state, payload) {
    state.pedido = payload;
  },
};
