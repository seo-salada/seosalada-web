import firebase from './../helpers/firebaseConfig';
/* eslint-disable prefer-template */
/* eslint-disable no-param-reassign */
/* eslint-disable no-named-as-default  */
export default{
  saveProdutor({ commit }, payload) {
    if (!payload.complemento) {
      payload.complemento = '';
    }
    return new Promise((resolve, reject) => {
      if (payload.id) {
        firebase.database().ref('produtores/' + payload.id).set(payload)
        .then((response) => {
          commit('SAVE_PRODUTOR', response);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      } else {
        const id = firebase.database().ref('produtores').push();
        payload.id = id.key;
        id.update(payload)
        .then((response) => {
          commit('SAVE_PRODUTOR', response);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }
    });
  },
  saveProduto({ commit }, payload) {
    const root = firebase.database().ref();
    let id = root.child('/produtos').push();
    if (payload.id !== undefined) {
      id = root.child('/produtos/' + payload.id);
    }
    payload.id = id.key;
    id.set(payload, (err) => {
      if (!err) {
        // const name = id.key;
        // payload.produtores.forEach((produtor) => {
        //   firebase.database().ref('produtores/' + produtor).child('produtos/' + name)
        //   .set(payload);
        // }, this);
      }
    });
  },
  uploadImagem({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const storage = firebase.storage();
      const storageRef = storage.ref('/images/' + payload.image.name);
      const storageRefCompressed = storage.ref('/images/thumbs/' + payload.image.name);
      const file = payload.image;
      const fileCompressed = payload.imageCompressed.split(',')[1];
      storageRefCompressed.putString(fileCompressed, 'base64').then((response) => {
        // eslint-disable-next-line
        console.log(response);
      }, (error) => {
        reject(error);
      });
      storageRef.put(file).then((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  },
  uploadImagemLabel({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const storage = firebase.storage();
      const storageRef = storage.ref('/labels/' + payload.imageUrl);
      const file = payload.image.split(',')[1];
      storageRef.putString(file, 'base64').then((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  },
  saveLabel({ commit }, payload) {
    const root = firebase.database().ref();
    let id = root.child('/labels').push();
    if (payload.id !== undefined) {
      id = root.child('/labels/' + payload.id);
    }
    payload.id = id.key;
    id.set(payload, (err) => {
      // eslint-disable-next-line
      console.log(err);
    });
  },
  saveBanner({ commit }, payload) {
    const root = firebase.database().ref();
    let id = root.child('/banners').push();
    if (payload.id !== undefined) {
      id = root.child('/banners/' + payload.id);
    }
    payload.id = id.key;
    id.set(payload, (err) => {
      // eslint-disable-next-line
      console.log(err);
    });
  },
  uploadImagemBanner({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const storage = firebase.storage();
      const storageRef = storage.ref('/banners/' + payload.image.name);
      const file = payload.image;
      if (payload.image.name) {
        storageRef.put(file).then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }

      const storageRefMobile = storage.ref('/banners/mobile/' + payload.imageMobile.name);
      const fileMobile = payload.imageMobile;
      if (payload.imageMobile.name) {
        storageRefMobile.put(fileMobile).then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }
    });
  },
  savePromocao({ commit }, payload) {
    return new Promise((resolve, reject) => {
      if (payload.id) {
        firebase.database().ref('promocoes/' + payload.id).update(payload)
        .then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      } else {
        const id = firebase.database().ref('promocoes').push();
        payload.id = id.key;
        // const nome = payload.nome;
        id.update(payload)
        .then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }
    });
  },
  updateCliente({ commit, state }, payload) {
    const db = firebase.database();
    const clientsRef = db.ref('clientes').child(payload.id);

    clientsRef.update(payload)
    .then((res) => {
      // eslint-disable-next-line
      console.log(res);
    });
  },
  saveCliente({ commit }, payload) {
    const auth = firebase.auth();
    auth.createUserWithEmailAndPassword(payload[0].email, payload[1])
    .then(
      (res) => {
        const db = firebase.database().ref('clientes').push();
        /* eslint-disable no-param-reassign */
        const cliente = payload[0];
        cliente.idAuth = res.uid;
        cliente.role = 'client';
        cliente.id = db.key;
        db.set(payload[0])
        .then((response) => {
          // eslint-disable-next-line
          console.log(response);
        });
      },
    ).catch((error) => {
      // eslint-disable-next-line
      console.log(error);
    });
  },
  saveCategoria({ commit }, payload) {
    return new Promise((resolve, reject) => {
      if (payload.id !== '') {
        firebase.database().ref('categorias/' + payload.id).update(payload)
        .then((response) => {
          commit('SAVE_CATEGORIA', response);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      } else {
        const id = firebase.database().ref('categorias').push();
        payload.id = id.key;
        id.update(payload)
        .then((response) => {
          commit('SAVE_CATEGORIA', response);
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }
    });
  },
  saveCidade({ commit }, payload) {
    return new Promise((resolve, reject) => {
      if (payload.id !== '') {
        firebase.database().ref('cidades/' + payload.id).update(payload)
        .then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      } else {
        const id = firebase.database().ref('cidades').push();
        payload.id = id.key;
        id.update(payload)
        .then((response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
      }
    });
  },
  deletar({ commit }, payload) {
    firebase.database().ref(payload.ref + '/' + payload.key).remove();
  },
  deletarProduto({ commit }, payload) {
    firebase.database().ref('produtos/' + payload.key).once('value').then(snap => snap.val().produtores.forEach(produtor =>
    firebase.database().ref('produtores/' + produtor + '/produtos/' + payload.key).set(null).then(firebase.database().ref('produtos/' + payload.key).set(null))));
  },
  getImagem({ commit }, payload) {
    // firebase.database().ref(payload.ref + '/' + payload.key).remove();
    // const id = root.child(payload.id).delete();
    // console.log(payload);
    firebase.database().ref('produtos/' + payload).once('value')
    .then(snapshot => firebase.storage().ref().child('images/' + snapshot.val().imageUrl).getDownloadURL())
    .then(url => commit('SAVE_IMAGEM', url));
  },
  getClientes({ commit }) {
    firebase.database().ref('clientes').once('value')
    .then(snapshot => commit('GET_CLIENTES', snapshot.val()));
  },
  pedido({ commit }, payload) {
    if (!payload.numero) {
      firebase.database().ref('ultimoPedido').once('value')
      .then((snap) => {
        const pedidoNumero = Number(snap.val()) + 1;
        payload.numero = pedidoNumero;
        firebase.database().ref('pedidos').push().set(payload);
        firebase.database().ref('ultimoPedido').set(pedidoNumero);
      });
    } else {
      firebase.database().ref('pedidos/' + payload.key).update(payload)
        .then((response) => {
          // eslint-disable-next-line
          console.log(response);
        }, (error) => {
          // eslint-disable-next-line
          console.log(error);
        });
    }
  },
  getProdutos({ commit }) {
    firebase.database().ref('produtos').once('value').then(snap => commit('GET_PRODUTOS', snap.val()));
  },
  getPedido({ commit }, payload) {
    firebase.database().ref(`pedidos/${payload}`).once('value').then(snap => commit('GET_PEDIDO', snap.val()));
  },
  updateStatus({ commit }, payload) {
    const db = firebase.database();
    db.ref(payload.ref)
    .orderByChild(payload.primaryKey)
    .equalTo(payload.id)
    .once('value')
    .then((snap) => {
      const ref = snap.val();
      const id = Object.keys(ref)[0];
      if (payload.motivoCancelamento) {
        db.ref(`${payload.ref}/${id}/motivoCancelamento`).set(payload.motivoCancelamento)
          .then(() => {
            db.ref(`${payload.ref}/${id}/status`).set(payload.status);
          });
      } else if (payload.sacolas) {
        db.ref(`clientes/${payload.sacolas.usuarioID}/sacolas`).once('value')
        .then((sacolaSnap) => {
          let sacolas = payload.sacolas.entregue - payload.sacolas.recebida;
          sacolas += sacolaSnap.val();
          db.ref(`clientes/${payload.sacolas.usuarioID}/sacolas`).set(sacolas).then(() => {
            db.ref(`${payload.ref}/${id}/status`).set(payload.status);
            db.ref(`${payload.ref}/${id}/pagamento`).set(payload.pagamento);
          });
        });
      } else {
        db.ref(`${payload.ref}/${id}/status`).set(payload.status);
      }
    });
  },
  changeDisponibility({ commit }, payload) {
    firebase.database().ref(`${payload.ref}/${payload.id}/disponivel`).set(payload.disponivel);
  },
  changeActive({ commit }, payload) {
    firebase.database().ref(`${payload.ref}/${payload.id}/ativo`).set(payload.ativo);
  },
  changeStar({ commit }, payload) {
    firebase.database().ref(`${payload.ref}/${payload.id}/destaque`).set(payload.destaque);
  },
};
