import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard';
import Login from '@/components/Login';

import PedidosList from '@/components/Pedidos/List';
import PedidosForm from '@/components/Pedidos/Form';

import ClientesList from '@/components/Clientes/List';
import ClientesForm from '@/components/Clientes/Form';

import ProdutoresList from '@/components/Produtores/List';
import ProdutoresForm from '@/components/Produtores/Form';

import ProdutosList from '@/components/Produtos/List';
import ProdutosForm from '@/components/Produtos/Form';

import CategoriasList from '@/components/Categorias/List';
import CategoriasForm from '@/components/Categorias/Form';

import CidadesList from '@/components/Cidades/List';
import CidadesForm from '@/components/Cidades/Form';

import AssinaturasList from '@/components/Assinaturas/List';
import AssinaturasForm from '@/components/Assinaturas/Form';

import PromocoesList from '@/components/Promocoes/List';
import PromocoesForm from '@/components/Promocoes/Form';

import BannersList from '@/components/Banners/List';
import BannersForm from '@/components/Banners/Form';

import LabelsList from '@/components/Labels/List';
import LabelsForm from '@/components/Labels/Form';

import CadastroList from '@/components/Cadastro/List';

import ListaCompras from '@/components/Listas/Compras';
import ListaRecibos from '@/components/Listas/Recibos';
import ListaEntregas from '@/components/Listas/Entregas';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Dashboard,
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/pedidos',
      name: 'Lista de Pedidos',
      component: PedidosList,
    },
    {
      path: '/pedidos/novo',
      name: 'Novo Pedido',
      component: PedidosForm,
    },
    {
      path: '/pedidos/:numero',
      name: 'Editar Pedido',
      component: PedidosForm,
    },
    {
      path: '/clientes',
      name: 'Lista de Clientes',
      component: ClientesList,
    },
    {
      path: '/clientes/novo',
      name: 'Novo Cliente',
      component: ClientesForm,
    },
    {
      path: '/clientes/:idcliente',
      name: 'Editar Cliente',
      component: ClientesForm,
    },
    {
      path: '/produtores',
      name: 'Lista de Produtores',
      component: ProdutoresList,
    },
    {
      path: '/produtores/novo',
      name: 'Novo Produtor',
      component: ProdutoresForm,
    },
    {
      path: '/produtores/:idprodutor',
      name: 'Editar Produtor',
      component: ProdutoresForm,
    },
    {
      path: '/produtos',
      name: 'Lista de Produtos',
      component: ProdutosList,
    },
    {
      path: '/produtos/novo',
      name: 'Novo Produto',
      component: ProdutosForm,
    },
    {
      path: '/produtos/:idproduto',
      name: 'Editar Produto',
      component: ProdutosForm,
    },
    {
      path: '/categorias',
      name: 'Lista de Categoria',
      component: CategoriasList,
    },
    {
      path: '/categorias/novo',
      name: 'Nova Categoria',
      component: CategoriasForm,
    },
    {
      path: '/categorias/:idcategoria',
      name: 'Editar Categoria',
      component: CategoriasForm,
    },
    {
      path: '/cidades',
      name: 'Lista de Cidade',
      component: CidadesList,
    },
    {
      path: '/cidades/novo',
      name: 'Nova Cidade',
      component: CidadesForm,
    },
    {
      path: '/cidades/:idcidade',
      name: 'Editar Cidade',
      component: CidadesForm,
    },
    {
      path: '/assinaturas',
      name: 'Lista de Assinaturas',
      component: AssinaturasList,
    },
    {
      path: '/assinaturas/novo',
      name: 'Nova Assinatura',
      component: AssinaturasForm,
    },
    {
      path: '/assinaturas/:idassinatura',
      name: 'Editar Assinatura',
      component: AssinaturasForm,
    },
    {
      path: '/promocoes',
      name: 'Lista de Promoções',
      component: PromocoesList,
    },
    {
      path: '/promocoes/novo',
      name: 'Nova Promoção',
      component: PromocoesForm,
    },
    {
      path: '/promocoes/:idpromocao',
      name: 'Editar Promoção',
      component: PromocoesForm,
    },
    {
      path: '/banners',
      name: 'Lista de Banners',
      component: BannersList,
    },
    {
      path: '/banners/novo',
      name: 'Novo Banner',
      component: BannersForm,
    },
    {
      path: '/banners/:idbanner',
      name: 'Editar Banner',
      component: BannersForm,
    },
    {
      path: '/labels',
      name: 'Lista de Labels',
      component: LabelsList,
    },
    {
      path: '/labels/novo',
      name: 'Novo Label',
      component: LabelsForm,
    },
    {
      path: '/labels/:idlabel',
      name: 'Editar Label',
      component: LabelsForm,
    },
    {
      path: '/cadastro/lista/:idcadastro',
      name: 'Listar Cadastro',
      component: CadastroList,
    },
    {
      path: '/listas/compras',
      name: 'Lista de Compras',
      component: ListaCompras,
    },
    {
      path: '/listas/recibos',
      name: 'Lista de Recibos',
      component: ListaRecibos,
    },
    {
      path: '/listas/entregas',
      name: 'Lista de Entregas',
      component: ListaEntregas,
    },
  ],
});
