// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueMaterial from 'vue-material';
import VueHighcharts from 'vue-highcharts';
import VueFire from 'vuefire';

import 'vue-material/dist/vue-material.css';
import 'material-design-iconic-font/dist/css/material-design-iconic-font.css';
import 'font-awesome/scss/font-awesome.scss';

import firebase from './helpers/firebaseConfig';
import App from './App';
import router from './router';
import './assets/css/core.scss';
import PageContent from './components/Shared/PageContent';
import SideBar from './components/Shared/SideBar';
import Loading from './components/Shared/Loading';
import store from './store/store';

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.use(VueMaterial);
Vue.use(VueHighcharts);
Vue.use(VueFire);

Vue.component('page-content', PageContent);
Vue.component('sideBar', SideBar);
Vue.component('loading', Loading);

const db = firebase.database();

Vue.material.registerTheme({
  default: {
    primary: 'green',
    accent: 'light-green',
    warn: 'brown',
  },
  blue: {
    primary: 'blue',
    accent: 'pink',
  },
  indigo: {
    primary: 'indigo',
    accent: 'pink',
  },
  brown: {
    primary: 'brown',
    accent: 'green',
  },
  purple: {
    primary: 'purple',
    accent: 'blue',
  },
  orange: {
    primary: 'orange',
    accent: 'purple',
  },
  green: {
    primary: 'green',
    accent: 'pink',
  },
  'light-blue': {
    primary: 'light-blue',
    accent: 'yellow',
  },
  teal: {
    primary: 'teal',
    accent: 'orange',
  },
  'blue-grey': {
    primary: 'blue-grey',
    accent: 'blue',
  },
  cyan: {
    primary: 'cyan',
    accent: 'pink',
  },
  red: {
    primary: 'red',
    accent: 'pink',
  },
  yellow: {
    primary: 'yellow',
    accent: 'amber',
  },
  white: {
    primary: 'white',
    accent: 'blue',
  },
  grey: {
    primary: {
      color: 'grey',
      hue: 300,
    },
    accent: 'indigo',
  },
  secondary: {
    primary: 'light-blue',
    accent: 'cyan',
    warn: 'deep-orange',
    background: 'white',
  },
  dashboard: {
    primary: 'red',
    accent: 'indigo',
    warn: 'green',
    background: 'amber',
  },
  graph: {
    primary: 'white',
    accent: 'indigo',
    warn: 'green',
    background: 'amber',
  },
});

/* eslint-disable no-new */
new Vue({
  router,
  store,
  db,
  firebase: {
    anArray: db.ref('url/to/my/collection'),
  },
  created() {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.$router.push('/login');
        return false;
      }
      firebase.database().ref('clientes')
        .orderByChild('idAuth')
        .equalTo(user.uid)
        .limitToFirst(1)
        .once('value', (snapshot) => {
          const userSession = snapshot.val();
          if (userSession && userSession[Object.keys(userSession)[0]].role !== 'admin') {
            firebase.auth().signOut();
            this.$router.push('/login');
          }
        });
      return true;
    });
  },
  el: '#app',
  render: h => h(App),
});
