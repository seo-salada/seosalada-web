import firebase from 'firebase';

/* eslint-disable no-unused-vars */
const configProd = {
  apiKey: 'AIzaSyCsBb3ko6H9V395oaQxd-Dx1_A7FK4xOPE',
  authDomain: 'seosalada-f9943.firebaseapp.com',
  databaseURL: 'https://seosalada-f9943.firebaseio.com',
  projectId: 'seosalada-f9943',
  storageBucket: 'seosalada-f9943.appspot.com',
  messagingSenderId: '254983556986',
};

const firebaseApp = firebase.initializeApp(configProd);

export default firebaseApp;
